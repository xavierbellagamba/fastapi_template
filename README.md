# FastAPI template

> This is a template for FastAPI microservices. Names need to be adjusted in all the README files ,the test files and some of the config files. 
> All the src files need to be adjusted for the new service.
> The Gitlab repo needs to be parametrized with the right secrets, protected branches etc. 

Developer: Xavier Bellagamba (xavier.bellagamba@outlook.com)
Current version: v1.0

## Introduction
Brief intro on what is the purpose of the service

It possesses the following functionalities:
- XXXXX

## Requirements
- python>=3.10.8
- - pydantic>=1.10.2
- fastapi>=0.95.2
- uvicorn>=0.20.0

## Endpoints
The miroservice has the following endpoints: 
- v1/XXXX GET: description

## Running the service
Running the service takes a three stp process:
1. Create a local folder that will be the mounted volume of the container
2. Build the image running ``` docker build --tag XXXXX .``` in the root folder of the project
3. Run the Docker container with the following parameters: 
    - Image name: XXXXX
    - Container name: XXXXX_1
    - Port: 8000:8000
    - Volume: {your local folder} mounted on ```/data```

    It can be done via command line with: 
    ```
    docker run -d \
        --name XXXXXX_1 \
        -p 8000:8000 \
        -v /path/to/your/local/folder:/data \
        -e API_KEY="your API key" \
        XXXXX
    ```
    Or using Docker Desktop for simplicity. 

The container should be tested to ensure it runs as expected. Please, look in the ```/doc``` folder for some examples.

## Using the service
All endpoints of the service are currently implemented as sync functions.
Calls to the microservice always expect a header containing the API key for security purposes ("API-key":"{your API key}").

Please refer to the ```/doc``` folder to see some examples related the use of the different endpoints.

## Repo structure
The repo contains the following folders:
- config: holdin the logging configuration
- doc: hosting some request examples that can help accelerate adoption
- requirements: holding the different and necessary requirements to run the service
- src/fastapi_teamplate: source code of the service
- tests: unit tests covering the application
