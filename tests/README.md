# Unit testing

To test locally, create ```test_0_setup.py``` in the ```/test``` directory. It should then contain the following: 

```
from os import environ

environ["API_KEY"] = {your API KEY for this service}
environ["POLYGON_KEY"] = {your Polygon.io API KEY}
```

To execute the tests, run the following command from the root directory: 

```
coverage run --source=src/fastapi_template -m pytest tests
```

And to see the current coverage, run:
```
coverage report --show-missing
```
