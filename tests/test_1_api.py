import pytest

import sys
sys.path.append("./src/fastapi_template")
from os import environ

from fastapi.testclient import TestClient
from src.fastapi_template.api import app

client = TestClient(app)

def test_get_health():
    headers = {"API-Key": environ["API_KEY"]}
    
    response = client.get("/v1/health", headers=headers)
    assert response.status_code == 200
    assert response.json() == {"status": "OK"}

def test_get_help():
    headers = {"API-Key": environ["API_KEY"]}
    
    response = client.get("/v1/help", headers=headers)
    assert response.status_code == 200
