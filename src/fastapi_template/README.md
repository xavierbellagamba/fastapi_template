# Source

This directory contains the source code of the service. 
It is structured as follows: 
- ```api.py```: contains the endpoints definition and is the file run within the container
- ```DataAcquisitionWorkflow.py```: defines the workflows to get the data from the Polygon.io API
- ```dto.py```: contains the data structures shared between mutliple components within the project
- ```utils.py```: contains the functions necessary to operate the service


