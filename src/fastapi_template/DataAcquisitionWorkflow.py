import logging
from datetime import datetime
from uuid import uuid4

import pandas as pd
from dto import PolygonRequest
from polygon2pandas.DataFetcher import DataFetcher
from utils import register_workflow


class DataAcquisitionWorkflow:
    def __init__(self):
        """
        Initialize the workflow object
        """
        self.timestamp = datetime.now().isoformat()
        self.status = "INITIALIZED"
        self.step = "CREATION"
        self.id = "ACQ-" + str(uuid4())
        # Could potentially be enhanced with pydantic
        self.data_validation_config = self._generate_data_validation_config()
        register_workflow(self.id, self.timestamp, "", self.step, self.status)

    def _generate_data_validation_config(self):
        """
        Generate the data validation of the input (simple one, only checks for
        presence).

        Returns
        -------
        dict
            A dictionary containing the elements that need to be validated at
            request reception
        """
        return {
            "financials": [
                ["cik", "ticker"],
                "from_dt",
                "to_dt",
                "report_period",
            ],
            "bars": [
                "ticker",
                "multiplier_time",
                "granularity",
                "from_dt",
                "to_dt",
            ],
            "details": ["ticker"],
        }

    def _validate_request_input(self, request_input: dict):
        """
        Validate the presence of the required attributes in the request

        Parameters
        ----------
        request_input: dict
            The request parameters that will be used to generate the request to
            Polygon.io

        Returns
        -------
        validation_status: dict
            The status and its companion message of the request validation
            process
        """
        validation_status = {"message": "No error", "status": "OK"}
        if "req_type" not in request_input.keys():
            validation_status["message"] = "Missing attribute: req_type"
            validation_status["status"] = "FAILED"

        for attribute in self.data_validation_config[
            request_input["req_type"]
        ]:
            if type(attribute) is list:
                for att_option in attribute:
                    found = False
                    for k, _ in request_input.items():
                        if att_option == k:
                            found = True
                            break
                    if found:
                        break
                else:
                    validation_status[
                        "message"
                    ] = "Missing attribute: {attr}".format(attr=attribute)
                    validation_status["status"] = "FAILED"
                    break
            else:
                for k, _ in request_input.items():
                    if attribute == k:
                        break
                else:
                    validation_status[
                        "message"
                    ] = "Missing attribute: {attr}".format(attr=attribute)
                    validation_status["status"] = "FAILED"
                    break

        return validation_status

    def _get_data(
        self, request_input: dict, polygon_key: str, datafetcher_config: dict
    ):
        """
        Sends the request to Polygon.io to get the required data.

        Parameters
        ----------
        request_input: dict
            The request input necessary to execute a query on Polygon.io
            following the polygon2pandas library guidelines
        polygon_key: str
            The Polygon.io API key
        datafetcher_config: dict
            The DataFetcher object config as specified in the polygon2pandas
            library guidelines

        Returns
        -------
        data_acquisition_response: dict
            A dictionary that contains the data, the status, the companion
            message for logging purposes.
        """
        data_acquisition_response = {
            "message": "",
            "status": "",
            "data": pd.DataFrame(),
        }
        data_fetcher = DataFetcher(polygon_key, datafetcher_config)
        try:
            data_acquisition_response["data"] = data_fetcher.get_data(
                request_input
            )
            data_acquisition_response["message"] = "Data acquired"
            data_acquisition_response["status"] = "OK"
        except ValueError:
            data_acquisition_response["data"] = pd.DataFrame()
            data_acquisition_response["message"] = "Data acquisition failed"
            data_acquisition_response["status"] = "FAILED"
        return data_acquisition_response

    def run_workflow(self, polygon_request: PolygonRequest):
        """
        Goes through the entire data acquisition workflow and registers each
        step into the workflow DB.

        Parameters
        ----------
        polygon_request: PolygonRequest (DTO)
            The request containing the API key to use, the parameters for the
            request and the parameters of the DataFetcher object (from the
            polygon2pandas library).

        Retruns
        -------
        dict
            Containing the data as pd.DataFrame, the status of the workflow,
            its last step and the companion message for logging purposes.
        """
        # Validate the input
        logging.info("Start running workflow {id_}".format(id_=self.id))
        validation_status = self._validate_request_input(
            polygon_request.request_input
        )
        self.timestamp = datetime.now().isoformat()
        self.step = "VALIDATION"
        if validation_status["status"] == "OK":
            self.status = "VALIDATED"
            register_workflow(
                self.id,
                self.timestamp,
                polygon_request.json(),
                self.step,
                self.status,
            )
        else:
            logging.warning(
                "Workflow {id_} failed at validation step".format(id_=self.id)
            )
            self.status = "FAILED"
            register_workflow(
                self.id,
                self.timestamp,
                polygon_request.json(),
                self.step,
                self.status,
            )
            return {
                "id": self.id,
                "timestamp": self.timestamp,
                "data": pd.DataFrame(),
                "status": self.status,
                "step": self.step,
                "message": validation_status["message"],
            }

        # Get the data from Polygon.io
        data_acquisition_response = self._get_data(
            polygon_request.request_input,
            polygon_request.polygon_key,
            polygon_request.datafetcher_config,
        )
        self.step = "DATA ACQUISITION"
        self.timestamp = datetime.now().isoformat()
        if len(data_acquisition_response["data"]) != 0:
            self.status = "DATA ACQUIRED"
            register_workflow(
                self.id,
                self.timestamp,
                polygon_request.json(),
                self.step,
                self.status,
            )
        else:
            self.status = "FAILED"
            logging.warning(
                "Workflow {id_} failed at data acquisition step".format(
                    id_=self.id
                )
            )
            register_workflow(
                self.id,
                self.timestamp,
                polygon_request.json(),
                self.step,
                self.status,
            )
            return {
                "id": self.id,
                "timestamp": self.timestamp,
                "data": pd.DataFrame(),
                "status": self.status,
                "step": self.step,
                "message": data_acquisition_response["message"],
            }

        # Return data
        logging.info(
            "Workflow {id_} successfully completed".format(id_=self.id)
        )
        return {
            "id": self.id,
            "timestamp": self.timestamp,
            "data": data_acquisition_response["data"],
            "status": self.status,
            "step": self.step,
            "message": "Request successfully executed",
        }
