from pydantic import BaseModel


class PolygonRequest(BaseModel):
    polygon_key: str
    datafetcher_config: dict
    request_input: dict
