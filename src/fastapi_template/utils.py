import logging
import logging.config
import os
import sqlite3
from os.path import exists
from pathlib import Path
from sqlite3 import Error as SQLError

script = """CREATE TABLE IF NOT EXISTS workflows (
    id text NOT NULL,
    timestamp text NOT NULL,
    request text NOT NULL,
    step text NOT NULL,
    status text NOT NULL
);
"""


def log_setup():
    """
    Function used by the main.py file at execution. It sets up the logging in
    a folder called logs for the entire pipeline. The config file is stored
    under config/logging.conf
    """
    if not os.path.isdir("./data"):
        os.mkdir("./data")

    logging.config.fileConfig(
        "./config/logging.conf", disable_existing_loggers=False
    )


def create_table():
    """
    Loads the table creation script given as an argument and runs it.
    """
    conn = create_connection_workflow_db()
    try:
        c = conn.cursor()
        c.execute(script)
    except SQLError:
        raise ValueError("Problem with SQL. Look at the traceback")


def db_setup():
    """
    Sets up the DB if it doesn't exist yet
    """
    if not exists(Path("./data/workflow.db")):
        logging.info("DB doesn't exist yet. Creating it...")
        create_table()


def create_connection_workflow_db() -> sqlite3.Connection:
    """
    Establishes the connection with the DB.

    Returns
    -------
    conn: sqlite3.Connection
        The connection to the DB
    """
    conn = None
    try:
        conn = sqlite3.connect("./data/workflow.db")
    except SQLError:
        raise ValueError("Connection with the DB not well established")

    return conn


def register_workflow(
    id_: str, timestamp: str, request: str, step: str, status: str
):
    """
    Registers the workflow as provided.

    Paramters
    ---------
    id_: str
        The workflow ID
    timestamp: str
        The timestamp given to that update
    request: str
        The request as a string
    step: str
        The current workflow step
    status: str
        The status of that step
    """
    conn = create_connection_workflow_db()
    sql_op = """INSERT INTO workflows(
        id, timestamp, request, step, status
        ) VALUES(?,?,?,?,?)"""
    sql_params = [id_, timestamp, request, step, status]

    try:
        c = conn.cursor()
        c.execute(sql_op, sql_params)
        conn.commit()

    except SQLError as e:
        logging.critical(e)
        raise ValueError("Problem with SQL. Look at the traceback")


def get_workflow_status(uuid_workflow: str):
    """
    Returns the last entry of the workflow with the provided uuid

    Parameters
    ----------
    uuid_workflow: str
        The ID of the workflow

    Returns
    -------
    dict
        Containing the id of the workflow, the timestamp of the last update,
        the step at which it is and the status of the step.
    """
    conn = create_connection_workflow_db()

    sql_op = """SELECT * FROM workflows
        WHERE id="{id_}";""".format(
        id_=uuid_workflow
    )

    try:
        c = conn.cursor()
        c.execute(sql_op)
        last_status = c.fetchall()[-1]

    except SQLError as e:
        logging.critical(e)
        raise ValueError("Problem with SQL. Look at the traceback")

    return {
        "id_": last_status[0],
        "timestamp": last_status[1],
        "step": last_status[3],
        "status": last_status[4],
    }
