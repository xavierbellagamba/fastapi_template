from datetime import datetime
from os import environ

from DataAcquisitionWorkflow import DataAcquisitionWorkflow
from dto import PolygonRequest
from fastapi import Depends, FastAPI, HTTPException, Security
from fastapi.security import APIKeyHeader
from utils import db_setup, get_workflow_status, log_setup, register_workflow

app = FastAPI()
api_key_header = APIKeyHeader(name="API-Key", auto_error=False)


log_setup()
db_setup()


API_KEY = environ["API_KEY"]


def get_api_key(api_key: str = Security(api_key_header)) -> str:
    """
    Returns the API key if the API is correct. Otherwise returns a 403.

    Parameters
    ----------
    api_key: str
        The provided API key

    Returns
    -------
    api_key: str
        The validated API key
    """
    if api_key != API_KEY:
        raise HTTPException(status_code=403, detail="Invalid API key")
    return api_key


@app.get("/")
def root():
    """
    Root endpoint. No particular functionality.
    """
    return {
        "message": "Welcome to the API. Please refer to the documentation \
            for available endpoints."
    }


@app.get("/v1/help", tags=["documentation"])
def get_help():
    """
    Provides help and documentation for the API.
    """
    endpoints = [
        {
            "path": "/v1/",
            "description": "Root endpoint. Returns a welcome message.",
        },
        {
            "path": "/v1/help",
            "description": "Displays API documentation and available \
                endpoints.",
        },
        {
            "path": "/v1/health",
            "description": "Endpoint for checking the health of the API.",
        },
        {
            "path": "/v1/polygon_request",
            "description": "POST: Retrieves the data using the provided \
            request.",
        },
        {
            "path": "/v1/polygon_request/{uuid}",
            "description": "GET: Retrieves the information about the request \
            with the indicatged uuid.",
        },
    ]
    return {"endpoints": endpoints}


@app.get("/v1/health", tags=["health"])
def get_health(api_key: str = Depends(get_api_key)):
    """
    Checks the health of the API.
    """
    return {"status": "OK"}


@app.get("/v1/polygon_request/{uuid_workflow}", tags=["polygon_request"])
def get_workflow_metadata(
    uuid_workflow: str, api_key: str = Depends(get_api_key)
):
    """
    Retrieves ticker information for the specified symbol.
    """
    return get_workflow_status(uuid_workflow)


@app.post("/v1/polygon_request", tags=["polygon_request"])
def request_new_data(
    polygon_request: PolygonRequest, api_key: str = Depends(get_api_key)
):
    """
    Creates a new data acquisition workflow and runs it based on the provided
    request
    """
    workflow = DataAcquisitionWorkflow()

    response = workflow.run_workflow(polygon_request)

    workflow.timestamp = datetime.now().isoformat()
    workflow.status = "COMPLETED"
    workflow.step = "FINALIZATION"

    register_workflow(
        workflow.id,
        workflow.timestamp,
        polygon_request.json(),
        workflow.step,
        workflow.status,
    )

    return response
