FROM python:3.10.12-slim

LABEL maintainer="xavier.bellagamba@outlook.com"
LABEL version="v0.1.0"

# Copy the necessary folders to the image
COPY ./requirements requirements
COPY ./scripts scripts
COPY ./src/fastapi_template .
COPY ./config config

# Check python install and upgrade pip
RUN python --version
RUN pip install --upgrade pip

# Install requirements (Pandas must be installed separately)
RUN pip install -r requirements/requirements.txt
RUN pip install requirements/polygon2pandas-0.1.4-py3-none-any.whl

# Volume
RUN mkdir ./data
VOLUME ./data

# Environment variables
ARG PROVIDED_API_KEY
ENV API_KEY=$PROVIDED_API_KEY

# Open ports
EXPOSE 8000:8000

# Run the API
CMD uvicorn api:app --host 0.0.0.0 --port 8000
