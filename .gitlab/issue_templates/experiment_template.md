## What
Briefly explain what the ticket is about (what is to be tested)

## Why
Briefly explain why the ticket is important and what benefits it brings

## How
Provide how it going to be implemented: dataset used and assumptions

### Assumptions
- Which are the assumptions made

### Datasets
- Provide a description of the datasets to be used

## Acceptance criteria
- [ ] List the different criteria that need to be fulfilled to accept this ticket as closed
